#! /bin/sh


# Step 1:
# Read source and target data and save as OpenNMT torchtext objects
# Train file is save as `data/exp.train.pt`, validation file `data/exp.valid.pt``
# mkdir dataset
python preprocess.py \
-train_src /data1/Adam/datasets/English2Chinese/tokenized_jieba_data/train.en \
-train_tgt /data1/Adam/datasets/English2Chinese/tokenized_jieba_data/train.zh \
-valid_src /data1/Adam/datasets/English2Chinese/tokenized_jieba_data/dev.en \
-valid_tgt /data1/Adam/datasets/English2Chinese/tokenized_jieba_data/dev.zh \
-save_data /data1/Adam/code/English2Chinese/dataset/jieba