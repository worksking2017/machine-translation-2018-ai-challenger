#! /bin/sh

# 计算BLEU并打印
chmod 777 ./tool/wrap_xml.pl
chmod 777 ./tool/chi_char_segment.pl
chmod 777 ./tool/mteval-v11b.pl
./tool/wrap_xml.pl zh /data1/Adam/datasets/English2Chinese/ai_challenger_MTEnglishtoChinese_validationset_20180823/ai_challenger_MTEnglishtoChinese_validationset_20180823_en.sgm pred_dev < outputs/pred_dev.txt > outputs/pred_dev.sgm

python ./tool/mt-score-main.py -rs /data1/Adam/datasets/English2Chinese/ai_challenger_MTEnglishtoChinese_validationset_20180823/ai_challenger_MTEnglishtoChinese_validationset_20180823_zh.sgm -hs outputs/pred_dev.sgm -ss /data1/Adam/datasets/English2Chinese/ai_challenger_MTEnglishtoChinese_validationset_20180823/ai_challenger_MTEnglishtoChinese_validationset_20180823_en.sgm --id pred_dev | tee score/pred_dev.score