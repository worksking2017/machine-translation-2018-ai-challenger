# #! /bin/sh

# #Download the dataset and put the dataset in ../raw_data file

# DATA_DIR= /data1/Adam/datasets/English2Chinese/tokenized_jieba_data/
# TMP_DIR= /data1/Adam/datasets/English2Chinese/
# # mkdir -p $DATA_DIR

# awk -F '\t' '{print $3}' /data1/Adam/datasets/English2Chinese/ai_challenger_MTEnglishtoChinese_trainingset_20180827/ai_challenger_MTEnglishtoChinese_trainingset_20180827.txt > /data1/Adam/datasets/English2Chinese/ai_challenger_MTEnglishtoChinese_trainingset_20180827/train.en
# awk -F '\t' '{print $4}' /data1/Adam/datasets/English2Chinese/ai_challenger_MTEnglishtoChinese_trainingset_20180827/ai_challenger_MTEnglishtoChinese_trainingset_20180827.txt > /data1/Adam/datasets/English2Chinese/ai_challenger_MTEnglishtoChinese_trainingset_20180827/train.zh

# # unwrap xml for valid data and test data  去除验证集、测试集中xml代码
# python prepare_data/unwrap_xml.py /data1/Adam/datasets/English2Chinese/ai_challenger_MTEnglishtoChinese_validationset_20180823/ai_challenger_MTEnglishtoChinese_validationset_20180823_zh.sgm >/data1/Adam/datasets/English2Chinese/valid.en-zh.zh
# python prepare_data/unwrap_xml.py /data1/Adam/datasets/English2Chinese/ai_challenger_MTEnglishtoChinese_validationset_20180823/ai_challenger_MTEnglishtoChinese_validationset_20180823_en.sgm >/data1/Adam/datasets/English2Chinese/valid.en-zh.en
python prepare_data/unwrap_xml.py /data1/Adam/datasets/English2Chinese/ai_challenger_MTEnglishtoChinese_testB_20180827_en.sgm >/data1/Adam/datasets/English2Chinese/testB.en


# # 去除验证集和测试集英文数据中的ID
# awk -F '\t' '{print $3}' /data1/Adam/datasets/English2Chinese/valid.en-zh.en > /data1/Adam/datasets/English2Chinese/valid.en
awk -F '\t' '{print $3}' /data1/Adam/datasets/English2Chinese/testB.en > /data1/Adam/datasets/English2Chinese/test.en

# # Prepare Data

# # Chinese words segmentation
# python prepare_data/jieba_cws.py /data1/Adam/datasets/English2Chinese/ai_challenger_MTEnglishtoChinese_trainingset_20180827/train.zh > /data1/Adam/datasets/English2Chinese/tokenized_jieba_data/train.zh #训练集中文分词化
# python prepare_data/jieba_cws.py /data1/Adam/datasets/English2Chinese/valid.en-zh.zh > /data1/Adam/datasets/English2Chinese/tokenized_jieba_data/dev.zh  #验证集中文分词化

# # activate python2.7,  Tokenize and Lowercase English training data
chmod 777 prepare_data/tokenizer.perl
# cat /data1/Adam/datasets/English2Chinese/ai_challenger_MTEnglishtoChinese_trainingset_20180827/train.en | prepare_data/tokenizer.perl -l en | tr A-Z a-z > /data1/Adam/datasets/English2Chinese/tokenized_jieba_data/train.en  #训练集英文tokenizer化
# cat /data1/Adam/datasets/English2Chinese/valid.en | prepare_data/tokenizer.perl -l en | tr A-Z a-z > /data1/Adam/datasets/English2Chinese/tokenized_jieba_data/dev.en  #验证集英文tokenizer化
cat /data1/Adam/datasets/English2Chinese/test.en | prepare_data/tokenizer.perl -l en | tr A-Z a-z > /data1/Adam/datasets/English2Chinese/tokenized_jieba_data/testB.en  #c测试集英文tokenizer化


# # ## activate python2.7, 去除低频词
# python prepare_data/build_dictionary.py /data1/Adam/datasets/English2Chinese/tokenized_jieba_data/train.en
# python prepare_data/build_dictionary.py /data1/Adam/datasets/English2Chinese/tokenized_jieba_data/train.zh

# trg_vocab_size=100000  #控制中文低频范围
# src_vocab_size=100000  #控制英文低频范围
# python prepare_data/generate_vocab_from_json.py /data1/Adam/datasets/English2Chinese/tokenized_jieba_data/train.en.json ${src_vocab_size} > /data1/Adam/datasets/English2Chinese/tokenized_jieba_data/vocab.en
# python prepare_data/generate_vocab_from_json.py /data1/Adam/datasets/English2Chinese/tokenized_jieba_data/train.zh.json ${trg_vocab_size} > /data1/Adam/datasets/English2Chinese/tokenized_jieba_data/vocab.zh
# rm -r /data1/Adam/datasets/English2Chinese/tokenized_jieba_data/train.*.json
