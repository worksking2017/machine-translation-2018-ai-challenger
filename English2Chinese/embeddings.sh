python ./tools/embeddings_to_torch.py \
-emb_file_enc /data1/Adam/code/English2Chinese/glove_dir/english_vocab_512.txt \
-emb_file_dec /data1/Adam/code/English2Chinese/glove_dir/dec_zh_vocab_512.txt \
-output_file /data1/Adam/code/English2Chinese/dataset/embeddings_512 \
-dict_file /data1/Adam/code/English2Chinese/dataset/jieba.vocab.pt