#! /bin/sh

# Step 2:
# Start training. Models are saved as `exp-model_acc_X_ppl_X_eX.pt`
# python train.py \
#         -pre_word_vecs_enc dataset/embeddings_512.enc.pt \
#         -pre_word_vecs_dec dataset/embeddings_512.dec.pt \
#         -data dataset/jieba \
#         -save_model ckpts/training_embddings_512_rnn \
#         -layers 7 -rnn_size 512 -word_vec_size 512  \
#         -encoder_type brnn -decoder_type rnn \
#         -train_steps 2000000  -max_generator_batches 2 -dropout 0.5 \
#         -batch_size 300 -batch_type tokens -normalization tokens -accum_count 3 \
#         -optim adam -adam_beta2 0.999 -decay_method noam -warmup_steps 15000 \
#         -max_grad_norm 0 -param_init 0  -param_init_glorot \
#         -save_checkpoint_steps 20000 -valid_steps 18000 \
#         -world_size 3 -gpu_ranks 0 1 2

# python  train.py \
#         -pre_word_vecs_enc dataset/embeddings_512.enc.pt \
#         -pre_word_vecs_dec dataset/embeddings_512.dec.pt \
#         -data dataset/jieba \
#         -save_model ckpts/training_transformer_embeddings \
#         -layers 4 -rnn_size 512 -word_vec_size 512 -transformer_ff 2048 -heads 8  \
#         -encoder_type transformer -decoder_type transformer -position_encoding \
#         -train_steps 200000  -max_generator_batches 2 -dropout 0.1 \
#         -batch_size 1024 -batch_type tokens -normalization tokens  -accum_count 2 \
#         -optim adam -adam_beta2 0.998 -decay_method noam -warmup_steps 8000 -learning_rate 2 \
#         -max_grad_norm 0 -param_init 0  -param_init_glorot \
#         -label_smoothing 0.1 -valid_steps 2000 -save_checkpoint_steps 2000 \
#         -world_size 4 -gpu_ranks 0 1 2 3

python train.py \
        -pre_word_vecs_enc dataset/embeddings.enc.pt \
        -pre_word_vecs_dec dataset/embeddings.dec.pt \
        -data dataset/jieba \
        -save_model ckpts/training_embddings_cnn \
        -layers 6 -rnn_size 512 -word_vec_size 300 \
        -encoder_type brnn -decoder_type rnn \
        -train_steps 500000  -max_generator_batches 2 -dropout 0.1 \
        -batch_size 2048 -batch_type tokens -normalization tokens  -accum_count 2 \
        -optim adam -adam_beta2 0.999 -decay_method noam -warmup_steps 18000 \
        -learning_rate 2 \
        -max_grad_norm 0 -param_init 0  -param_init_glorot \
        -label_smoothing 0.1 -save_checkpoint_steps 20000 -valid_steps 18000 \
        -world_size 3 -gpu_ranks 0 1 2

# -train_from ckpts/training_embddings_cnn_step_85000.pt \